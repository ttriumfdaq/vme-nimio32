## Generated SDC file "VME-NIMIO32.sdc"

## Copyright (C) 1991-2012 Altera Corporation
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, Altera MegaCore Function License 
## Agreement, or other applicable license agreement, including, 
## without limitation, that your use is for the sole purpose of 
## programming logic devices manufactured by Altera and sold by 
## Altera or its authorized distributors.  Please refer to the 
## applicable agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 12.0 Build 232 07/05/2012 Service Pack 1.07 SJ Full Version"

## DATE    "Fri Sep  7 15:05:50 2012"

##
## DEVICE  "EP1C6Q240C6"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {altera_reserved_tck} -period 100.000 -waveform { 0.000 50.000 } [get_ports {altera_reserved_tck}]
create_clock -name {50MHz_Osc}  -period 20.000 -waveform { 0.000 10.000 } [get_ports {50MHz_Osc}]
create_clock -name {50MHz_Osc1} -period 20.000 -waveform { 0.000 10.000 } [get_ports {50MHz_Osc1}]

create_clock -name {Scaler00} -period 3.333 -waveform { 0.000 1.500 } [get_nets {sc_mux_a*result_node[0]}]
create_clock -name {Scaler01} -period 3.333 -waveform { 0.000 1.500 } [get_nets {sc_mux_a*result_node[1]}]
create_clock -name {Scaler02} -period 3.333 -waveform { 0.000 1.500 } [get_nets {sc_mux_a*result_node[2]}]
create_clock -name {Scaler03} -period 3.333 -waveform { 0.000 1.500 } [get_nets {sc_mux_a*result_node[3]}]
create_clock -name {Scaler04} -period 3.333 -waveform { 0.000 1.500 } [get_nets {sc_mux_b*result_node[0]}]
create_clock -name {Scaler05} -period 3.333 -waveform { 0.000 1.500 } [get_nets {sc_mux_b*result_node[1]}]
create_clock -name {Scaler06} -period 3.333 -waveform { 0.000 1.500 } [get_nets {sc_mux_b*result_node[2]}]
create_clock -name {Scaler07} -period 3.333 -waveform { 0.000 1.500 } [get_nets {sc_mux_b*result_node[3]}]
create_clock -name {Scaler08} -period 3.333 -waveform { 0.000 1.500 } [get_nets {sc_mux_c*result_node[0]}]
create_clock -name {Scaler09} -period 3.333 -waveform { 0.000 1.500 } [get_nets {sc_mux_c*result_node[1]}]
create_clock -name {Scaler10} -period 3.333 -waveform { 0.000 1.500 } [get_nets {sc_mux_c*result_node[2]}]
create_clock -name {Scaler11} -period 3.333 -waveform { 0.000 1.500 } [get_nets {sc_mux_c*result_node[3]}]
create_clock -name {Scaler12} -period 3.333 -waveform { 0.000 1.500 } [get_nets {sc_mux_d*result_node[0]}]
create_clock -name {Scaler13} -period 3.333 -waveform { 0.000 1.500 } [get_nets {sc_mux_d*result_node[1]}]
create_clock -name {Scaler14} -period 3.333 -waveform { 0.000 1.500 } [get_nets {sc_mux_d*result_node[2]}]
create_clock -name {Scaler15} -period 3.333 -waveform { 0.000 1.500 } [get_nets {sc_mux_d*result_node[3]}]

create_clock -name {Scaler31} -period 3.333 -waveform { 0.000 1.500 } [get_nets {sc_in31_LCELL}]

create_clock -name {Ext_clock} -period 25.0 [get_pins {tsc_ext_clk_TFF|clk}]

#**************************************************************
# Create Generated Clock
#**************************************************************

create_generated_clock -name {pll0_clock_100MHz} -source [get_pins {pll0|altpll_component|pll|inclk[0]}] -duty_cycle 50.000 -multiply_by 2 -master_clock {50MHz_Osc} [get_pins {pll0|altpll_component|pll|clk[0]}] 
create_generated_clock -name {pll0_clock_40MHz}  -source [get_pins {pll0|altpll_component|pll|inclk[0]}] -duty_cycle 50.000 -multiply_by 4 -divide_by 5 -master_clock {50MHz_Osc} [get_pins { pll0|altpll_component|pll|clk[1] }] 

create_generated_clock -name {pll0_clock_20MHz}  -source [get_pins {pll0|altpll_component|pll|clk[1]}] -duty_cycle 50.000 -multiply_by 1 -divide_by 2 -master_clock {pll0_clock_40MHz} [get_keepers {clock_20MHz_TFF}] 

create_generated_clock -name {pll1_clock_0} -source [get_pins {pll1|altpll_component|pll|inclk[0]}] -duty_cycle 50.000 -multiply_by 1 -divide_by 1 -master_clock {50MHz_Osc1} [get_pins {pll1|altpll_component|pll|clk[0]}] 
create_generated_clock -name {pll1_clock_1}  -source [get_pins {pll1|altpll_component|pll|inclk[0]}] -duty_cycle 50.000 -multiply_by 4 -divide_by 1 -master_clock {50MHz_Osc1} [get_pins { pll1|altpll_component|pll|clk[1] }] 

#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************



#**************************************************************
# Set Input Delay
#**************************************************************



#**************************************************************
# Set Output Delay
#**************************************************************



#**************************************************************
# Set Clock Groups
#**************************************************************

set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}]

set_clock_groups -asynchronous -group [get_clocks {pll0_clock_20MHz}]
set_clock_groups -asynchronous -group [get_clocks {pll0_clock_40MHz}]
set_clock_groups -asynchronous -group [get_clocks {pll0_clock_100MHz}]

set_clock_groups -asynchronous -group [get_clocks {pll1_clock_0}]
set_clock_groups -asynchronous -group [get_clocks {pll1_clock_1}]

set_clock_groups -asynchronous -group [get_clocks {Scaler*}]

set_clock_groups -asynchronous -group [get_clocks {Ext_clock}]

#**************************************************************
# Set False Path
#**************************************************************

#set_false_path  -from  [get_clocks {pll0_clock_100MHz}] -to   [get_clocks {Scaler*}]
#set_false_path  -to    [get_clocks {pll0_clock_100MHz}] -from [get_clocks {Scaler*}]

#set_false_path  -from  [get_clocks {pll0_clock_100MHz}] -to   [get_clocks {Scaler*}]

#set_false_path  -from  [get_keepers {Scaler*lpm_counter*safe_q*}] -to [get_keepers {Scaler*lpm_fifo*}]

#set_false_path  -from  [get_clocks {altpll0:pll0|altpll:altpll_component|_clk1}]  -to  [get_clocks {altpll0:pll0|altpll:altpll_component|_clk0}]
#set_false_path  -from  [get_clocks {altpll0:pll0|altpll:altpll_component|_clk0}]  -to  [get_clocks {altpll0:pll0|altpll:altpll_component|_clk1}]

#set_false_path -to [get_keepers {inst3}]
#set_false_path -from [get_keepers {inst3}]
#set_false_path -to [get_keepers {inst9}]

set_false_path -to [get_keepers {*signaltap*}]

set_false_path -from [get_ports {FP_NIMIN*}]
set_false_path -from [get_ports {FP_LVDS_RX*}]

set_false_path -from [get_ports {SER_*}]
set_false_path -from [get_ports {VME-*}]
set_false_path -from [get_ports {SMB_*}]
set_false_path -to [get_ports {SER_*}]
set_false_path -to [get_ports {VME-*}]
set_false_path -to [get_ports {SMB_*}]
set_false_path -to [get_ports {FP_NIMOUT*}]
set_false_path -to [get_ports {FP_LED*}]
set_false_path -to [get_ports {FP_VME_LEDn}]
set_false_path -to [get_ports {FPGA_nCONFIG}]

#set_false_path -from {D[*]}

set_false_path -from [get_keepers {reg4*}]
set_false_path -from [get_keepers {reg5*}]
set_false_path -from [get_keepers {reg10*}]
#set_false_path -from [get_keepers {reg11*}]
set_false_path -from [get_keepers {reg12*}]
set_false_path -from [get_keepers {reg16*}]
set_false_path -from [get_keepers {reg16bis*}]
set_false_path -from [get_keepers {reg17*}]
set_false_path -from [get_keepers {reg48*}]
set_false_path -from [get_keepers {reg51*}]
set_false_path -from [get_keepers {reg52*}]
set_false_path -from [get_keepers {reg55*}]
set_false_path -from [get_keepers {reg56*}]
set_false_path -from [get_keepers {reg57*}]
set_false_path -from [get_keepers {reg58*}]
set_false_path -from [get_keepers {reg59*}]
set_false_path -to   [get_keepers {reg59*}]
set_false_path -from [get_keepers {reg62*}]
set_false_path -from [get_keepers {reg63*}]

#set_false_path -to [get_cells {reg4*}]

#set_false_path -from [get_keepers {CMD_DFF*}]

set_false_path -to [get_keepers {tsc_ext_clk_TFF}]
#set_false_path -to [get_keepers {tsc_ext_clk_MUX}]
set_false_path -to   [get_keepers {tsc_clock_latch_DFF}]
set_false_path -from [get_keepers {tsc_clock_latch_DFF}]
set_false_path -to   [get_keepers {Scaler*ExtLatchSync_DFF}]
#set_false_path -to   [get_keepers {OR_ext_ResetTS*}]
#set_false_path -from [get_keepers {OR_ext_ResetTS*}]
set_false_path -from [get_keepers {scaler_enable_DFF*}]
set_false_path -to [get_keepers {led_bus_sync*}]
set_false_path -from [get_keepers {clear_scaledown_DFF*}]

#set_false_path -from [get_pins {FP_NIMIN[*]}]
#set_false_path -from [get_pins {FP_NIMIN[0]}]
#set_false_path -from [get_pins {FP_NIMIN[6]}]
#set_false_path -from [get_pins {FP_NIMIN[7]}]

#set_false_path -to [get_pins -compatibility_mode {*scaledown_counter*|clk}]
#set_false_path -to {Scaledown:Scaledown_trinat_bgo|in}

#set_instance_assignment -name SYNCHRONIZER_IDENTIFICATION "FORCED" -to trinat_ionmcp_sync_DFF

create_clock -name {fake_clock_1} -period 1000000 [get_pins {led_sel_DFF*|clk}]
create_clock -name {fake_clock_2} -period 1000000 [get_pins {TS_Reset|pfs_latch|clk}]
create_clock -name {fake_clock_2a} -period 1000000 [get_pins {tsc_clock_latch_DFF|clk}]
create_clock -name {fake_clock_2b} -period 1000000 [get_pins {ext_reset_pfs|pfs_latch|clk}]

#create_clock -name {fake_clock_2x1} -period 1000000 [get_pins {inst88*|clk}]
#create_clock -name {fake_clock_2x1} -period 1000000 [get_pins {inst88*|clock}]
#create_clock -name {fake_clock_2x1} -period 1000000 [get_pins {inst88|clock}]
#create_clock -name {fake_clock_2x2} -period 1000000 [get_pins {inst3|clk}]

create_clock -name {fake_clock_3} -period 1000000 [get_pins {s1249_accept_dff|clk}]
create_clock -name {fake_clock_4} -period 1000000 [get_pins {s1249_pileup_dff|clk}]
create_clock -name {fake_clock_5} -period 1000000 [get_pins {s1249_GateLatch|clk}]

create_clock -name {fake_clock_6} -period 1000000 [get_pins {alpha_busy_extend_DFF|clk}]
create_clock -name {fake_clock_7} -period 1000000 [get_pins {alpha_reset_pfs|pfs_latch|clk}]

create_clock -name {fake_clock_8} -period 1000000 [get_pins {dragon_AdcLatch|clk}]
create_clock -name {fake_clock_9} -period 1000000 [get_pins {dragon_tail_latch_DFF*|clk}]

#create_clock -name {fake_clock_10} -period 1000000 [get_pins {trinat_TrigLatch|clk}]
#create_clock -name {fake_clock_11} -period 1000000 [get_pins {trinat_ionmcp_latch|clk}]

#create_clock -name {fake_clock_12} -period 1000000 {trinat_emcp_gate_DFF}
#create_clock -name {fake_clock_13} -period 1000000 {trinat_emcp_uv_DFF}
#create_clock -name {fake_clock_14} -period 1000000 {trinat_sc_gate_DFF}
#create_clock -name {fake_clock_15} -period 1000000 {trinat_sc_long_gate_DFF}
#create_clock -name {fake_clock_16} -period 1000000 {trinat_sc12_DFF}
#create_clock -name {fake_clock_17} -period 1000000 {trinat_sc12_led_DFF}
#create_clock -name {fake_clock_18} -period 1000000 {trinat_sc12_emcp_DFF}
#create_clock -name {fake_clock_19} -period 1000000 {trinat_rmcp_DFF}
#create_clock -name {fake_clock_19a} -period 1000000 {trinat_rmcp_DFF10}
#create_clock -name {fake_clock_20} -period 1000000 {trinat_rmcp_gate_DFF}
#create_clock -name {fake_clock_21} -period 1000000 {trinat_rmcp_uv_DFF}
#create_clock -name {fake_clock_22} -period 1000000 {trinat_rmcp_coinc_DFF}
#create_clock -name {fake_clock_23} -period 1000000 {trinat_rmcp_latch}
create_clock -name {fake_clock_24} -period 1000000 [get_pins {trinat_ScLongLatch|clk}]
create_clock -name {fake_clock_25} -period 1000000 [get_pins {trinat_ScLatch|clk}]
create_clock -name {fake_clock_26} -period 1000000 [get_pins {trinat_eMcpLatch|clk}]
#create_clock -name {fake_clock_27} -period 1000000 {trinat_emcp_trigF_DFF}

create_clock -name {fake_clock_28a1} -period 1000000 [get_pins -no_case -compatibility_mode *Scaledown*trinat_bgo*counter*clk]
create_clock -name {fake_clock_28a2} -period 1000000 [get_pins -no_case -compatibility_mode *Scaledown*trinat_bgo*scaledown_reset_DFF|clk]
create_clock -name {fake_clock_28b1a} -period 1000000 [get_pins -no_case -compatibility_mode *Scaledown*trinat_emcp_a*counter*clk]
create_clock -name {fake_clock_28b2a} -period 1000000 [get_pins -no_case -compatibility_mode *Scaledown*trinat_emcp_a*scaledown_reset_DFF|clk]
create_clock -name {fake_clock_28b1b} -period 1000000 [get_pins -no_case -compatibility_mode *Scaledown*trinat_emcp_b*counter*clk]
create_clock -name {fake_clock_28b2b} -period 1000000 [get_pins -no_case -compatibility_mode *Scaledown*trinat_emcp_b*scaledown_reset_DFF|clk]
create_clock -name {fake_clock_28c1} -period 1000000 [get_pins -no_case -compatibility_mode *Scaledown*trinat_sc12a*counter*clk]
create_clock -name {fake_clock_28c2} -period 1000000 [get_pins -no_case -compatibility_mode *Scaledown*trinat_sc12a*scaledown_reset_DFF|clk]
create_clock -name {fake_clock_28d1} -period 1000000 [get_pins -no_case -compatibility_mode *Scaledown*trinat_sc12b*counter*clk]
create_clock -name {fake_clock_28d2} -period 1000000 [get_pins -no_case -compatibility_mode *Scaledown*trinat_sc12b*scaledown_reset_DFF|clk]
create_clock -name {fake_clock_28e1} -period 1000000 [get_pins -no_case -compatibility_mode *Scaledown*trinat_rmcp*counter*clk]
create_clock -name {fake_clock_28e2} -period 1000000 [get_pins -no_case -compatibility_mode *Scaledown*trinat_rmcp*scaledown_reset_DFF|clk]
create_clock -name {fake_clock_29} -period 1000000 [get_pins {trinat_emcp_trigF_sync_DFF|clk}]
create_clock -name {fake_clock_30} -period 1000000 [get_pins {trinat_emcp_trigF_DFF|clk}]
create_clock -name {fake_clock_31} -period 1000000 [get_pins {trinat_emcp_uv_DFF|clk}]
create_clock -name {fake_clock_32} -period 1000000 [get_pins {trinat_rmcp_DFF|clk}]
create_clock -name {fake_clock_33} -period 1000000 [get_pins {trinat_rmcp_DFF10|clk}]
create_clock -name {fake_clock_34} -period 1000000 [get_pins {trinat_rmcp_coinc_DFF|clk}]
create_clock -name {fake_clock_35} -period 1000000 [get_pins {trinat_rmcp_uv_DFF|clk}]
create_clock -name {fake_clock_36} -period 1000000 [get_pins {trinat_sc12_DFF|clk}]
create_clock -name {fake_clock_37} -period 1000000 [get_pins {trinat_sc12_emcp_DFF|clk}]
create_clock -name {fake_clock_38} -period 1000000 [get_pins {trinat_sc12_led_DFF|clk}]
create_clock -name {fake_clock_39} -period 1000000 [get_pins {trinat_rmcp_gate_DFF|clk}]
create_clock -name {fake_clock_40} -period 1000000 [get_pins {trinat_sc_long_gate_DFF|clk}]
create_clock -name {fake_clock_41} -period 1000000 [get_pins {trinat_vf48_DFF|clk}]
create_clock -name {fake_clock_42a} -period 1000000 [get_pins {trinat_trigA_latch|clk}]
create_clock -name {fake_clock_43b} -period 1000000 [get_pins {trinat_trigB_latch|clk}]
create_clock -name {fake_clock_44c} -period 1000000 [get_pins {trinat_trigC_latch|clk}]
create_clock -name {fake_clock_45d} -period 1000000 [get_pins {trinat_trigD_latch|clk}]
create_clock -name {fake_clock_46e} -period 1000000 [get_pins {trinat_trigE_latch|clk}]
create_clock -name {fake_clock_47f} -period 1000000 [get_pins {trinat_trigF_latch|clk}]
create_clock -name {fake_clock_48g} -period 1000000 [get_pins {trinat_trigRR_latch|clk}]
create_clock -name {fake_clock_48h} -period 1000000 [get_pins {trinat_trigH_latch|clk}]
create_clock -name {fake_clock_49} -period 1000000 [get_pins {trinat_tdc_stop_DFF|clk}]
create_clock -name {fake_clock_50} -period 1000000 [get_pins {trinat_sc_gate_DFF|clk}]
create_clock -name {fake_clock_51} -period 1000000 [get_pins {trinat_emcp_gate_DFF|clk}]
create_clock -name {fake_clock_52} -period 1000000 [get_pins {trinat_rmcp_latch|clk}]
create_clock -name {fake_clock_53} -period 1000000 [get_pins {trinat_trigI_DFF|clk}]
create_clock -name {fake_clock_54} -period 1000000 [get_pins {trinat_trigI_latch|clk}]
create_clock -name {fake_clock_55} -period 1000000 [get_pins {trinat_trigJ_DFF|clk}]
create_clock -name {fake_clock_56} -period 1000000 [get_pins {trinat_trigJ_latch|clk}]

set_clock_groups -asynchronous -group [get_clocks {fake_clock_*}]

#set_false_path -from {fake_clock_14} -to {fake_clock_13}
#set_false_path -from {fake_clock_14} -to {fake_clock_16}
#set_false_path -from {fake_clock_14} -to {fake_clock_17}
#set_false_path -from {fake_clock_14} -to {fake_clock_18}
#set_false_path -from {fake_clock_15} -to {fake_clock_13}
#set_false_path -from {fake_clock_15} -to {fake_clock_16}
#set_false_path -from {fake_clock_15} -to {fake_clock_17}
#set_false_path -from {fake_clock_15} -to {fake_clock_18}
#set_false_path -from {fake_clock_27} -to {fake_clock_29}

set_false_path -from {fake_clock_30} -to {fake_clock_29}
set_false_path -from {fake_clock_40} -to {fake_clock_40}
set_false_path -from {fake_clock_40} -to {fake_clock_50}
set_false_path -from {fake_clock_50} -to {fake_clock_40}
set_false_path -from {fake_clock_50} -to {fake_clock_50}

set_false_path -from {fake_clock_28a1} -to {fake_clock_28a2}
set_false_path -from {fake_clock_28b1a} -to {fake_clock_28b2a}
set_false_path -from {fake_clock_28b1b} -to {fake_clock_28b2b}
set_false_path -from {fake_clock_28c1} -to {fake_clock_28c2}
set_false_path -from {fake_clock_28d1} -to {fake_clock_28d2}
set_false_path -from {fake_clock_28e1} -to {fake_clock_28e2}

set_false_path -from {fake_clock_28a2} -to {fake_clock_28a1}
set_false_path -from {fake_clock_28b2a} -to {fake_clock_28b1a}
set_false_path -from {fake_clock_28b2b} -to {fake_clock_28b1b}
set_false_path -from {fake_clock_28c2} -to {fake_clock_28c1}
set_false_path -from {fake_clock_28d2} -to {fake_clock_28d1}
set_false_path -from {fake_clock_28e2} -to {fake_clock_28e1}

#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

