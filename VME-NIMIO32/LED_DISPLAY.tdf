TITLE "Dsiplay control for TLC5928 16-bit LED Display Driver (2x16 bits)";
include "lpm_counter.inc";
include "lpm_shiftreg.inc";

SUBDESIGN LED_DISPLAY
(
	sysclk 				: INPUT;
	Update_Display		: INPUT;
	LED_DATA[31..0]		: INPUT;
	resetN				: INPUT;	

	LED_LATCH			: OUTPUT;
	LED_SCK				: OUTPUT;
	LED_SDO				: OUTPUT;
)
VARIABLE

LED_ShiftReg: 		lpm_shiftreg 	WITH (LPM_WIDTH = 32, LPM_DIRECTION = "LEFT");
LED_counter:		lpm_counter		WITH (LPM_WIDTH = 5);
	
	led_display:	MACHINE 

					WITH STATES (
				 		idle,
						load_data,
						shift_data,
						sck0,
						sck1,
						sck2,
						next_bit,
						last_SCK,
						latch_data);

BEGIN
 	DEFAULTS
		LED_ShiftReg.load 	= gnd;
		LED_ShiftReg.enable = gnd;
		LED_ShiftReg.shiftin = vcc;
  	END DEFAULTS;
  	
  	% connect bit counter ports %
  	LED_counter.clock 	= sysclk;
  	LED_counter.aclr	= idle;
  	LED_counter.cnt_en	= sck0;
  	
  	% connect shift register ports %
  	LED_ShiftReg.clock = sysclk;
  	LED_ShiftReg.data[] = LED_DATA[];
  	LED_ShiftReg.aclr	= latch_data;

  	% connect output ports %
  	LED_SCK 	= sck1 or last_SCK;
  	LED_SDO 	= LED_ShiftReg.shiftout;
  	LED_LATCH 	= latch_data;

  	
	% state machine assignments %
	led_display.clk		= sysclk;
	led_display.reset	= !resetN;

CASE	led_display IS

	WHEN	idle	=>
			IF 		Update_Display THEN			% check for update command %		
					led_display = load_data;
			ELSE
					 led_display = idle;
			END IF;
	
	WHEN	load_data =>
			LED_ShiftReg.load 	= VCC;			% LOAD shift register with display data %
			LED_ShiftReg.enable = VCC;
			led_display = shift_data;
			
	WHEN	shift_data =>
			LED_ShiftReg.enable = GND;			% shift out first bit to SDO pin%
			led_display		= sck2;
				
	WHEN 	sck2 =>								% set up data on SDO pin %
			led_display		= sck1;
			
	WHEN 	sck1 =>								% Rising edge of SCK %
			led_display		= sck0;
				
	WHEN	sck0 =>
			LED_ShiftReg.enable = VCC;			% shift out next bit to SDO pin %
			led_display		= next_bit;			% Falling edge of SCK %
			
	WHEN	next_bit =>

			IF LED_counter.cout == 1 THEN
				led_display = last_SCK;			% clock in last bit on SDO %
			ELSE
				led_display = sck1;				% else keep shifting out data until the end of the SR data %
			END IF;
			
	WHEN last_SCK =>
			led_display = latch_data;			% one more SCK for last bit %
			
	WHEN	latch_data =>						% latch data into TLC5928 display driver %
			led_display = idle;				
	
	WHEN OTHERS	=>
				led_display	= idle;	
END CASE;

END;

