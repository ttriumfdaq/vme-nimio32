## Generated SDC file "VME-PPG32.sdc"

## Copyright (C) 1991-2012 Altera Corporation
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, Altera MegaCore Function License 
## Agreement, or other applicable license agreement, including, 
## without limitation, that your use is for the sole purpose of 
## programming logic devices manufactured by Altera and sold by 
## Altera or its authorized distributors.  Please refer to the 
## applicable agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 12.0 Build 263 08/02/2012 Service Pack 2 SJ Full Version"

## DATE    "Wed Oct 10 15:32:58 2012"

##
## DEVICE  "EP3C40Q240C8"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {altera_reserved_tck} -period 100.000 -waveform { 0.000 50.000 } [get_ports {altera_reserved_tck}]
create_clock -name {50MHz_Osc} -period 20.000 -waveform { 0.000 10.000 } [get_ports {50MHz_Osc}]


#**************************************************************
# Create Generated Clock
#**************************************************************

create_generated_clock -name {clock_100MHz} -source [get_ports {50MHz_Osc}] -multiply_by 2 -master_clock {50MHz_Osc} [get_pins {pll0|altpll_component|auto_generated|pll1|clk[0]}] 
create_generated_clock -name {clock_20MHz} -source [get_ports {50MHz_Osc}] -multiply_by 2 -divide_by 5 -master_clock {50MHz_Osc} [get_pins {pll0|altpll_component|auto_generated|pll1|clk[1]}] 
create_generated_clock -name {clock_40MHz} -source [get_ports {50MHz_Osc}] -multiply_by 4 -divide_by 5 -master_clock {50MHz_Osc} [get_pins {pll0|altpll_component|auto_generated|pll1|clk[2]}] 


#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************



#**************************************************************
# Set Input Delay
#**************************************************************



#**************************************************************
# Set Output Delay
#**************************************************************



#**************************************************************
# Set Clock Groups
#**************************************************************

set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}] 
set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}] 
set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}] 
set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}] 


#**************************************************************
# Set False Path
#**************************************************************

set_false_path  -from  [get_clocks {clock_100MHz}]  -to  [get_clocks {clock_40MHz}]
set_false_path  -from  [get_clocks {clock_20MHz}]  -to  [get_clocks {clock_100MHz}]
set_false_path  -from  [get_clocks {clock_40MHz}]  -to  [get_clocks {clock_100MHz}]
set_false_path  -from  [get_clocks {50MHz_Osc}]  -to  [get_clocks {clock_100MHz}]
set_false_path -to [get_keepers {*signaltap*}]


#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

